Authorpane Popup
-------------------------------------------------------------

INTRODUCTION
-------------------------------------------------------------
It helps to show author information of node as a popup manner.

INSTALLATION
-------------------------------------------------------------
1. Copy the entire author_pane module directory into your 
   normal directory for modules,
   
	 usually sites/all/modules.

2. Enable the Author Pane module in ?q=admin/build/modules.

3. Go to 
   
   admin/config/development/author_pane_popup
   
   Enable the content types you want to show author information as popup.

DEMO
-------------------------------------------------------------
Goto http://www.developerdoc.com
