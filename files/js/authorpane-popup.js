/**
 * @file
 * js which loads author information as popup
*/
(function ($) {
  Drupal.behaviors.authorpane_popup = {
  attach: function (context, settings) {
    var app_hide;
    jQuery('.user-info-popup').mouseover(function(){
      jQuery('.authorpane-popup-load-content').remove();
      window.clearTimeout(app_hide);
      jQuery('body').append('<div class="authorpane-popup-load-content"><img src="' + Drupal.settings.authorpane_popup.ajax_loader + '" /></div>');
      var el  = jQuery(this);
      var pos = jQuery(el).offset();
      var eWidth = jQuery(el).outerWidth();
      var mWidth = jQuery('.authorpane-popup-load-content').outerWidth();
      var left = 3 + (pos.left + eWidth - mWidth) + "px";
      var top = 3 + pos.top + "px";
      jQuery('.authorpane-popup-load-content').css({
        position: 'absolute',
        zIndex: 5000,
        left: left,
        top: top
     });
     var pop_uid = jQuery(this).attr('id').split('-');
     jQuery('.authorpane-popup-load-content').show();
     jQuery.ajax({
      url : Drupal.settings.authorpane_popup.authorpane_popup_path + '/' + pop_uid[3],
      success: function(response){
        jQuery('.authorpane-popup-load-content').addClass('authorpane-popup-container');
        jQuery(".authorpane-popup-load-content").html(response);
      }
     });
   });
  jQuery('.user-info-popup').mouseout(function(){
    app_hide = window.setTimeout("jQuery('.authorpane-popup-load-content').remove();","1000");
  });
  jQuery('.authorpane-popup-load-content').live('mouseover',function(){
    window.clearTimeout(app_hide);
  });
  jQuery('.authorpane-popup-load-content').live('mouseout',function(){
    app_hide = window.setTimeout("jQuery('.authorpane-popup-load-content').remove();","1000");
  });
 }
 }
})(jQuery);
